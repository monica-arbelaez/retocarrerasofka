package modelo;

import casoUSo.Logica;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import javax.swing.JOptionPane;
import vista.Vista;

public class Conductor implements Comparable {
    private ArrayList<Conductor> listaJugador = new ArrayList();
    private int contadorTiradas;
    private String nombreConductor;
    private Vista miBienvenida;
    private int primerLugar;
    Logica log = new Logica();
    int longitud ;
 

    public ArrayList llenarArray() {

        listaJugador.add(new Conductor(0, "Gepardo", 0));
        listaJugador.add(new Conductor(0, "Flash", 0));
        listaJugador.add(new Conductor(0, "Azrael", 0));
        listaJugador.add(new Conductor(0, "Gamora", 0));
        listaJugador.add(new Conductor(0, "Thor", 0));
            
        return listaJugador;
        
    }
    
    public Conductor() {
        this.contadorTiradas = 0;
        this.nombreConductor = "";
        this.primerLugar = 0;
    }
        public Conductor(int contadorTiradas, String nombreConductor, int primerLugar) {
        this.contadorTiradas = contadorTiradas;
        this.nombreConductor = nombreConductor;
        this.primerLugar = primerLugar;
    }

    @Override
    public int compareTo(Object t) {
        int contadorTiradasDado = ((Conductor) t).getContadorTiradas();
        return this.contadorTiradas - contadorTiradasDado;
    }
    
    public ArrayList<Conductor> listaPodio( ArrayList listaOrdenada, int longitud){
        
           for(int i=0; i < listaJugador.size(); i++){
               contadorTiradas = log.cantidadTiradasDado(longitud);
               listaJugador.get(i).setContadorTiradas(contadorTiradas);
           }
             Collections.sort(listaJugador);

             System.out.println("el jugador : "+ listaJugador.get(0).getNombreConductor()+" posicion # 1");
             System.out.println("el jugador : "+ listaJugador.get(1).getNombreConductor()+" posicion # 2");
             System.out.println("el jugador : "+ listaJugador.get(2).getNombreConductor()+" posicion # 3");
             
             listaJugador.get(0).setPrimerLugar(primerLugar + 1);
            System.out.print("el Ganador  : "+ listaJugador.get(0).getNombreConductor()); 
            System.out.println(" tiene :" + listaJugador.get(0).getPrimerLugar()+ " tiunfos");
             
        return listaJugador;
    }
    
    
    public void BienvenidaUsuario(){
        miBienvenida = new Vista ();
          JOptionPane.showMessageDialog(null, "Usted ha seleccionado a:  " + listaJugador.get(miBienvenida.Bienvenida()-1).getNombreConductor());
    
    } 
       public int getContadorTiradas() {
        return contadorTiradas;
    }

    public void setContadorTiradas(int contadorTiradas) {
        this.contadorTiradas = contadorTiradas;
    }

    public String getNombreConductor() {
        return nombreConductor;
    }

    public void setNombreConductor(String nombreConductor) {
        this.nombreConductor = nombreConductor;
    }

    public Vista getMiBienvenida() {
        return miBienvenida;
    }

    public void setMiBienvenida(Vista miBienvenida) {
        this.miBienvenida = miBienvenida;
    }

    public int getPrimerLugar() {
        return primerLugar;
    }

    public void setPrimerLugar(int primerLugar) {
        this.primerLugar = primerLugar;
    }

}
