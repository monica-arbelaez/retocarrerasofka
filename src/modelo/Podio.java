
package modelo;


public class Podio {
    private String nombre;
    private int posicion1;
    private int posicion2;
    private int posicion3;
   

    public Podio() {
        this.nombre = "";
        this.posicion1 = 0;
        this.posicion2 = 0;
        this.posicion3 = 0;
    }
    
    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPosicion1() {
        return posicion1;
    }

    public void setPosicion1(int posicion1) {
        this.posicion1 = posicion1;
    }

    public int getPosicion2() {
        return posicion2;
    }

    public void setPosicion2(int posicion2) {
        this.posicion2 = posicion2;
    }

    public int getPosicion3() {
        return posicion3;
    }

    public void setPosicion3(int posicion3) {
        this.posicion3 = posicion3;
    }
}
