
package modelo;

import javax.swing.JOptionPane;


public class Pista {
    private  String[] nombrePista = {"Playa", "Montaña", "Ciudad", "Pueblo", "Desierto"};
    private int longitudPista;
    
    public int pistaJuego(int lg){
        JOptionPane.showMessageDialog(null, "Su pista  " + nombrePista [lg-1] + " fue creado exitosamente");
        return lg;  
    }
    
    public void iniciarJuego(){
        JOptionPane.showMessageDialog(null, "INCIAR LA CARRERA....>>>");
        
    }
    public Pista() {
        this.longitudPista = 0;
    }

    public int getLongitudPista() {
        return longitudPista;
    }

    public void setLongitudPista(int longitudPista) {
        this.longitudPista = longitudPista;
    }
    
    
}
