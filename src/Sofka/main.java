
package Sofka;


import java.util.ArrayList;
import modelo.Carril;
import modelo.Carro;
import modelo.Conductor;
import modelo.Pista;
import vista.Vista;


public class main {
    
      public static void main(String[] args) {
          
          Vista vista = new Vista();
          Conductor miCond = new Conductor();
          Carro miCarro = new Carro();
          Pista miPista = new Pista();
          Carril miCarril = new Carril();
          ArrayList <Conductor> listaCond;
          
            
          listaCond = miCond.llenarArray();
//          Bienvenida al juego y orden de los juagdores
          miCond.BienvenidaUsuario();
//          **Elegir carro
          miCarro.CarroJuador(vista.elegirCarro());
//          **Elegir Pista y la distancia por parametros
            int longitud  = vista.elegirDistancia();
          miPista.setLongitudPista(longitud);
//          **Elegir carril
           miCarril.miCarril(vista.elegirCarril());
//          **Iniciar carrera
           miPista.iniciarJuego();
           miCond.listaPodio(listaCond, longitud);

      }

}
    

